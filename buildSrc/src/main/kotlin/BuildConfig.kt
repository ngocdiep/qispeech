object GradleBuildConfig {
  val compileSdk = 28
  val applicationId = "com.khikon.qispeech"
  val minSdk = 23
  val targetSdk = 28
  val versionCode = 1
  val versionName = "1.0"
  val multidexEnabled = true
  val useSupportLibrary = true

  val testRunner = "androidx.test.runner.AndroidJUnitRunner"
}