object Versions {
  val gradle = "3.1.4"
  val protobuf = "0.8.5"
  val kotlin = "1.2.51"
  val kotshi = "1.0.4"
  val kotlinSerialization = "0.6.2"

  val retrofit = "2.4.0"
  val okhttp = "3.11.0"

  val rxAndroid = "2.1.0"
  val rxJava = "2.2.0"
  val rxKotlin = "2.3.0"

  val ktx = "1.0.0-alpha3"
  val playServices = "16.0.1"
  val dagger = "2.18"

  val androidxCore = "1.0.0"
  val archCore = "2.0.0" // support androidx
  val constraintLayout = "1.1.2" // support androidx
  val room = "2.0.0" // support androidx
  val multidex = "2.0.0" // support androidx

  val jodaTime = "2.9.9"
  val gson = "2.8.2"
  val timber = "4.6.0"
  val crashlytics = "2.9.5"

  val grpc = "1.15.1"

  val junit = "4.12"
  val testRunner = "1.1.0-alpha3" // support androidx
  val testRules = "1.1.0-alpha3" // support androidx
  val espresso = "3.1.0-alpha3" // support androidx
  val orchestrator = "1.1.0-alpha3" // support androidx
  val assertj = "3.9.1"
  val truth = "0.39"
  val mockito = "1.5.0"
  val robolectric = "3.8"
  val powerMock = "1.6.6"
  val uiautomator = "2.2.0-alpha1"
  val speech = "0.67.0-beta"
}

/**
 * Group for kotlin's dependencies
 * */
object Kotlin {
  val runtime = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Versions.kotlin}"
  val serialization = "org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Versions.kotlinSerialization}"
  val kotshiApi = "se.ansman.kotshi:api:${Versions.kotshi}"
  val kotshiCompiler = "se.ansman.kotshi:compiler:${Versions.kotshi}" // kapt
}

/**
 * Group for androidx migration
 * */
object AndroidX {
  val appcompat = "androidx.appcompat:appcompat:${Versions.androidxCore}"
  val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
  val material = "com.google.android.material:material:${Versions.androidxCore}"
  val recyclerview = "androidx.recyclerview:recyclerview:${Versions.androidxCore}"


  val lifecycle = "androidx.lifecycle:lifecycle-extensions:${Versions.archCore}"
  val streams = "androidx.lifecycle:lifecycle-reactivestreams:${Versions.archCore}"
  val archTest = "androidx.arch.core:core-testing:${Versions.archCore}"

  val room = "androidx.room:room-runtime:${Versions.room}"
  val roomCompiler = "androidx.room:room-compiler:${Versions.room}" // kapt
  val roomRx = "androidx.room:room-rxjava2:${Versions.room}"

  val multidex = "androidx.multidex:multidex:${Versions.multidex}"
  val multidex_instrument = "androidx.multidex:multidex-instrumentation:${Versions.multidex}"
}

/**
 * Group for network modules
 * */
object Network {
  val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
  val converter = "com.squareup.retrofit2:converter-moshi:${Versions.retrofit}"
  val adapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit}"
  val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttp}"
  val logging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp}"
}

/**
 * Group for dagger dependency
 * */
object Dagger {
  val core = "com.google.dagger:dagger:${Versions.dagger}"
  val compiler = "com.google.dagger:dagger-compiler:${Versions.dagger}" // kapt
  val processor = "com.google.dagger:dagger-android-processor:${Versions.dagger}" // kapt
  val android = "com.google.dagger:dagger-android:${Versions.dagger}"
  val support = "com.google.dagger:dagger-android-support:${Versions.dagger}" // Won't need if migrate to AndroidX
}

/**
 * Group for all other dependency libraries
 * */
object Libraries {
  val rxAndroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroid}"
  val rxJava = "io.reactivex.rxjava2:rxjava:${Versions.rxJava}"
  val rxKotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlin}"

  val timber = "com.jakewharton.timber:timber:${Versions.timber}"
  val crashlytics = "com.crashlytics.sdk.android:crashlytics:${Versions.crashlytics}@aar"
  val grpcOkhttp = "io.grpc:grpc-okhttp:${Versions.grpc}"
  val speech = "com.google.cloud:google-cloud-speech:${Versions.speech}"
}

/**
 * Group for all testing dependency libraries
 * */
object TestLibs {
  // General Unit Test
  val junit = "junit:junit:${Versions.junit}"
  val assertj = "org.assertj:assertj-core:${Versions.assertj}"
  val truth = "com.google.truth:truth:${Versions.truth}"

  // Mocking Unit Test
  val mockito = "com.nhaarman:mockito-kotlin:${Versions.mockito}"
  val mockWeb = "com.squareup.okhttp3:mockwebserver:${Versions.okhttp}"
  val robolectric = "org.robolectric:robolectric:${Versions.robolectric}"
  val roomTest = "androidx.room:room-testing:${Versions.room}" // androidTestImplementation

  // Instrument Test
  val runner = "androidx.test:runner:${Versions.testRunner}"
  val rules = "androidx.test:rules:${Versions.testRules}"

  val orchestrator = "androidx.test:orchestrator:${Versions.orchestrator}" // androidTestUtil
  val espresso = "androidx.test.espresso:espresso-core:${Versions.espresso}"
  val espressoIntent = "androidx.test.espresso:espresso-intents:${Versions.espresso}"
  val uiautomator = "androidx.test.uiautomator:uiautomator-v18:${Versions.uiautomator}"
}