## Note: 
- Please DON'T upgrade gradle and android plugin version to fit to Android Studio 3.2, because Android Studio 3.2 still isn't stable enough to support `kotlin-dsl` and will cause errors while synchronizing.

## Dependencies:
- Google Cloud SDK, environment variable GOOGLE_APPLICATION_CREDENTIALS
- Languages: Kotlin
- Pattern: MVVM
- Libraries: LiveData, Dagger, Retrofit, Rx, Room, AndroidX Material Design
- Test: Junit, Mockito

## Project modules:
There are 2 modules: `buildSrc` and `app`

+ `buildSrc` is used with `kotlin-dsl` for supporting add dependencies, reducing string messy.
+ `app` is the main application.

## Dagger modules:
- CommonModule: For general component providers
- AppModule: Specific to application, to separate with instrument test module
- ActivityModule: Declare Activity providers
- ViewModelModule: Declare ViewModel providers
- RepositoryModule & UsecaseModule: Specific for Repository and Usecase

## Application flows:

### Fetch dictionary list when launch app or `Fetch Dictionary` from OptionsMenu
- Fetch dictionary list from remote 
- Save to local database

### Speech recognition
+ Check record permission
+ Record audio and start streaming to Speech API
+ Match received string to the list from database. If match:

    - Vibrate
    - Highlight the matched row
    - Plus frequency by 1 for that string

