package com.khikon.qispeech.common

import android.content.Context
import android.util.Log
import com.crashlytics.android.Crashlytics
import com.khikon.qispeech.BuildConfig
import com.khikon.qispeech.data.log.Report
import io.fabric.sdk.android.Fabric
import timber.log.Timber
import javax.inject.Inject

interface AppInitial {
  fun initCrashlytics(context: Context)
  fun initTimber()
}

class SpeechAppInitial @Inject constructor(private val report: Report) : AppInitial {
  override fun initCrashlytics(context: Context) {
    Fabric.with(context, Crashlytics())
  }

  override fun initTimber() = if (BuildConfig.DEBUG) Timber.plant(
      Timber.DebugTree()) else Timber.plant(CrashReportingTree(report))
}

open class CrashReportingTree @Inject constructor(private val report: Report) : Timber.Tree() {
  override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
    if (priority == Log.VERBOSE || priority == Log.DEBUG) {
      return
    }

    report.logPriority(priority, tag, message)

    if (t != null) {
      if (priority == Log.ERROR) {
        report.logError(t)
      } else if (priority == Log.WARN) {
        report.logWarning(t)
      }
    }
  }
}