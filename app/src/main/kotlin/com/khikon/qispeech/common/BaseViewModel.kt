package com.khikon.qispeech.common

import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {
  val disposable: CompositeDisposable = CompositeDisposable()

  override fun onCleared() {
    disposable.dispose()
    super.onCleared()
  }
}