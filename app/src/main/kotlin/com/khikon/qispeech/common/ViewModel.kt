package com.khikon.qispeech.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.khikon.qispeech.di.ApplicationScoped
import dagger.MapKey
import javax.inject.Inject
import javax.inject.Provider
import kotlin.reflect.KClass

enum class DataState { LOADING, SUCCESS, ERROR, NONE }

data class Data<out T> constructor(
    var state: DataState, val model: T? = null, val message: String? = null)

@Suppress("UNCHECKED_CAST")
@ApplicationScoped
class ViewModelFactory @Inject constructor(private val viewModels: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) : ViewModelProvider.Factory {
  override fun <T : ViewModel> create(modelClass: Class<T>): T = viewModels[modelClass]?.get() as T
}

@MustBeDocumented
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER)
@Retention(AnnotationRetention.RUNTIME)
@MapKey
annotation class ViewModelKey(val value: KClass<out ViewModel>)