package com.khikon.qispeech.common

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.core.app.ActivityCompat
import com.khikon.qispeech.R
import com.khikon.qispeech.view.MessageDialogFragment

abstract class AudioPermissionActivity : ViewModelActivity() {
  companion object {
    private val PERMISSIONS = arrayOf(Manifest.permission.RECORD_AUDIO)
    private const val REQUEST_RECORD_AUDIO_PERMISSION = 200
    private const val FRAGMENT_MESSAGE_DIALOG = "message_dialog"
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    checkAudioPermission()
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION) {
      if (permissions.size == 1 && grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
        onAudioPermissionGranted()
      } else {
        showAudioPermissionDialog()
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
  }

  internal fun checkAudioPermission() {
    when {
      // Permission is granted -> good to go
      ActivityCompat.checkSelfPermission(this, PERMISSIONS[0]) == PackageManager.PERMISSION_GRANTED -> onAudioPermissionGranted()

      // Should show Rationale request if it is ever denied or reached never ask again case
      ActivityCompat.shouldShowRequestPermissionRationale(this, PERMISSIONS[0]) -> showRationaleAudioPermissionDialog()

      // Request permission normally
      else -> ActivityCompat.requestPermissions(this, PERMISSIONS, REQUEST_RECORD_AUDIO_PERMISSION)
    }
  }

  internal fun isAudioEnabled() = ActivityCompat.checkSelfPermission(this, PERMISSIONS[0]) == PackageManager.PERMISSION_GRANTED

  abstract fun onAudioPermissionGranted()

  abstract fun onAudioPermissionError()

  internal open fun showAudioPermissionDialog() {
    MessageDialogFragment.newInstance(getString(R.string.message_audio_permission))
        .setDialogListener(object : MessageDialogFragment.Listener {
          override fun onPositiveButtonClicked() {
            ActivityCompat.requestPermissions(this@AudioPermissionActivity, PERMISSIONS, REQUEST_RECORD_AUDIO_PERMISSION)
          }

          override fun onMessageDialogCancelled() {
            showRationaleAudioPermissionDialog()
          }
        }).show(supportFragmentManager, FRAGMENT_MESSAGE_DIALOG)
  }

  internal open fun showRationaleAudioPermissionDialog() {
    MessageDialogFragment.newInstance(getString(R.string.message_audio_rationale))
        .setDialogListener(object : MessageDialogFragment.Listener {
          override fun onPositiveButtonClicked() {
            openAppSettings()
          }

          override fun onMessageDialogCancelled() {
            onAudioPermissionError()
          }
        }).show(supportFragmentManager, FRAGMENT_MESSAGE_DIALOG)
  }

  private fun openAppSettings() {
    val intent = Intent()
    intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
    val uri = Uri.fromParts("package", packageName, null)
    intent.data = uri
    startActivity(intent)
  }
}