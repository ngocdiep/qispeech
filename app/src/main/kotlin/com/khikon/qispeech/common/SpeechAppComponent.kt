package com.khikon.qispeech.common

import com.khikon.qispeech.data.repository.RepositoryModule
import com.khikon.qispeech.data.usecase.UsecaseModule
import com.khikon.qispeech.di.*
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector

@ApplicationScoped
@Component(
    modules = [CommonModule::class, RepositoryModule::class, UsecaseModule::class, ViewModelModule::class, AndroidInjectionModule::class, ActivityModule::class, AppModule::class])
interface SpeechAppComponent : AndroidInjector<SpeechApp> {
  @Component.Builder
  abstract class Builder : AndroidInjector.Builder<SpeechApp>()
}