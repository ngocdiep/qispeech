package com.khikon.qispeech.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.khikon.qispeech.R
import dagger.android.AndroidInjection
import javax.inject.Inject

abstract class ViewModelActivity : AppCompatActivity() {
  @Inject
  lateinit var viewModelFactory: ViewModelProvider.Factory

  override fun onCreate(savedInstanceState: Bundle?) {
    AndroidInjection.inject(this)
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_empty)
    onSetupViewModel()
  }

  protected open fun onSetupViewModel() {}
}