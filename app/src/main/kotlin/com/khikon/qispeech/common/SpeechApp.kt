package com.khikon.qispeech.common

import android.content.Context
import androidx.multidex.MultiDex
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Inject

class SpeechApp : DaggerApplication() {
  @Inject lateinit var appInitial: AppInitial

  override fun applicationInjector(): AndroidInjector<out DaggerApplication> = DaggerSpeechAppComponent.builder().create(
      this)

  override fun attachBaseContext(base: Context) {
    super.attachBaseContext(base)
    MultiDex.install(this)
  }

  override fun onCreate() {
    super.onCreate()
    initDependencies()
  }

  private fun initDependencies() {
    appInitial.initCrashlytics(baseContext)
    appInitial.initTimber()
  }
}