package com.khikon.qispeech.ui.main

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.khikon.qispeech.R
import com.khikon.qispeech.common.AudioPermissionActivity
import com.khikon.qispeech.util.observe
import com.khikon.qispeech.util.withViewModel
import com.khikon.qispeech.view.RecordButton
import kotlinx.android.synthetic.main.view_dictionary.*
import kotlinx.android.synthetic.main.view_record.*

class MainActivity : AudioPermissionActivity(), RecordButton.OnRecordListener {
  private lateinit var mainViewModel: MainViewModel

  override fun setContentView(layoutResID: Int) = super.setContentView(R.layout.activity_main)

  override fun onPause() {
    super.onPause()
    pauseRecord()
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menuInflater.inflate(R.menu.menu_main, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    return when (item.itemId) {
      R.id.action_fetch -> {
        mainViewModel.fetchDictionaryList(true)
        true
      }
      else -> super.onOptionsItemSelected(item)
    }
  }

  override fun onSetupViewModel() {
    withViewModel<MainViewModel>(viewModelFactory) {
      this@MainActivity.mainViewModel = this

      // Setup view attributes
      setupRecordButton()
      setupBottomSheet()
      setupRecyclerView()

      // Observe change of LiveData
      observe(dictionarySpeechLiveData) {
        val matchedPosition = it?.matchedPosition ?: NO_INDEX

        // Update dictionary adapter binding
        (rv_dictionary.adapter as DictionaryAdapter).setDictionaries(it?.dictionaryList, matchedPosition)

        // Vibrate if word is matched
        if (matchedPosition != NO_INDEX) vibrate(500)

        // Bind speech text, change text color to R.color.accentColor if there is error
        if (it?.speech != null) {
          val speech = it.speech!!
          when {
            speech.error == null -> {
              text_hint.setText(R.string.hint_speech_result)
              text_result.text = speech.word
              text_result.setTextColor(getColor(R.color.secondaryDarkColor))
            }
            else -> {
              text_result.text = speech.error
              text_result.setTextColor(getColor(R.color.accentColor))
            }
          }
        }
      }

      observe(recording) {
        // Change hint when recording
        text_hint.setText(if (it == true) R.string.hint_listening else R.string.hint_view_dictionary)
        text_result.text = ""
      }

      // Fetch dictionary list from remote
      mainViewModel.fetchDictionaryList(false)
    }
  }

  private fun setupRecordButton() {
    record_button.setRecordListener(this)
  }

  private fun setupBottomSheet() {
    val bottomSheetBehavior = BottomSheetBehavior.from<View>(bottom_sheet_dictionary)
    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

    bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
      override fun onSlide(bottomsheet: View, slideOffset: Float) {
        image_navigator.visibility = View.GONE
      }

      @SuppressLint("SwitchIntDef")
      override fun onStateChanged(bottomsheet: View, newState: Int) {
        when (newState) {
          // When BottomSheet collapsed, show navigator icon to swipe up
          BottomSheetBehavior.STATE_COLLAPSED -> onUpdateBottomSheetState(true)

          // When BottomSheet expanded, hide navigator icon
          BottomSheetBehavior.STATE_EXPANDED -> {
            pauseRecord()
            onUpdateBottomSheetState(false, R.drawable.ic_close_white_24dp)
          }

          // Reset views' state for all other cases
          else -> {
            pauseRecord()
            onUpdateBottomSheetState(false)
          }
        }
      }
    })

    // Enable click to collapse/expand for the title
    title_dictionary.setOnClickListener {
      when (bottomSheetBehavior.state) {
        BottomSheetBehavior.STATE_COLLAPSED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED

        BottomSheetBehavior.STATE_EXPANDED -> bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
      }
    }
  }

  private fun onUpdateBottomSheetState(showNavigator: Boolean, titleDrawableStart: Int = 0) {
    if (showNavigator) {
      val transY = resources.getDimensionPixelSize(R.dimen.navigator_size).toFloat()
      title_dictionary.translationY = -transY
      title_dictionary.animate().translationY(0f).setDuration(100).start()
      image_navigator.visibility = View.VISIBLE
    } else {
      image_navigator.visibility = View.GONE
    }
    title_dictionary.setCompoundDrawablesRelativeWithIntrinsicBounds(titleDrawableStart, 0, 0, 0)
  }

  private fun setupRecyclerView() {
    rv_dictionary.layoutManager = LinearLayoutManager(baseContext)
    rv_dictionary.adapter = DictionaryAdapter()
  }

  private fun vibrate(duration: Long) {
    val vibrator = getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      vibrator.vibrate(VibrationEffect.createOneShot(duration, VibrationEffect.DEFAULT_AMPLITUDE))
    } else {
      // Deprecated in API 26
      vibrator.vibrate(duration)
    }
  }

  private fun pauseRecord() {
    // Reset recordButton state and pause stream
    record_button.cancelRecord()
    mainViewModel.pauseStreamSpeechService()
  }

  override fun onRecordStart() {
    when {
      // If record audio permission is enable, start to record
      isAudioEnabled() -> mainViewModel.startStreamSpeechService()
      // Else, require record audio permission
      else -> {
        record_button.cancelRecord()
        checkAudioPermission()
      }
    }
  }

  override fun onRecordCancel() {
    mainViewModel.pauseStreamSpeechService()
  }

  override fun onAudioPermissionGranted() {
  }

  override fun onAudioPermissionError() {
  }
}