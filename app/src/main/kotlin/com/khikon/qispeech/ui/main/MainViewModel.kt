package com.khikon.qispeech.ui.main

import android.content.res.Resources
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import com.khikon.qispeech.R
import com.khikon.qispeech.common.BaseViewModel
import com.khikon.qispeech.data.remote.model.Dictionary
import com.khikon.qispeech.data.usecase.DictionaryUsecase
import com.khikon.qispeech.service.speech.SpeechService
import com.khikon.qispeech.service.speech.StreamListener
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

const val NO_INDEX = -1

data class Speech(val word: String?, val error: String?)
data class DictionarySpeech(var dictionaryList: List<Dictionary>?, var speech: Speech?) {
  var matchedPosition: Int = NO_INDEX

  init {
    if (dictionaryList != null && speech?.word != null) {
      for (index in 0 until dictionaryList!!.size) {
        if (speech!!.word!!.trim().equals(dictionaryList!![index].word.trim(), true)) {
          matchedPosition = index
          break
        }
      }
    }
  }
}

class MainViewModel @Inject constructor(val resources: Resources, val dictionaryUsecase: DictionaryUsecase, var speechService: SpeechService?) : BaseViewModel(), StreamListener {
  val dictionarySpeechLiveData = MutableLiveData<DictionarySpeech>()
  val recording = MutableLiveData<Boolean>()

  fun fetchDictionaryList(reset: Boolean) {
    disposable.add(dictionaryUsecase.fetchDictionaries()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({ it -> onDictionaryListUpdated(it, reset) },
            { e -> e.printStackTrace() }))
  }

  @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
  fun onDictionaryListUpdated(list: List<Dictionary>?, reset: Boolean = false) {
    val speech = if (reset) null else dictionarySpeechLiveData.value?.speech
    val dictionarySpeech = DictionarySpeech(list, speech)
    dictionarySpeechLiveData.postValue(dictionarySpeech)
  }

  fun startStreamSpeechService() {
    speechService?.startStream(this)
    recording.postValue(true)
  }

  fun pauseStreamSpeechService() {
    speechService?.pauseStream()
    recording.postValue(false)
  }

  override fun onStreamResponse(value: String) {
    val dictionarySpeech = onSpeechUpdated(value, null)
    if (dictionarySpeech.matchedPosition != NO_INDEX) plusFrequency(value)
  }

  override fun onStreamError(t: Throwable) {
    t.printStackTrace()
    onSpeechUpdated(null, resources.getString(R.string.message_speech_error))
  }

  private fun onSpeechUpdated(value: String?, error: String?): DictionarySpeech {
    val list = dictionarySpeechLiveData.value?.dictionaryList
    val dictionarySpeech = DictionarySpeech(list, Speech(value, error))
    dictionarySpeechLiveData.postValue(dictionarySpeech)
    return dictionarySpeech
  }

  @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
  fun plusFrequency(value: String) {
    disposable.add(dictionaryUsecase.plusFrequency(value, 1)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe({ it -> onDictionaryListUpdated(it) },
            { e -> e.printStackTrace() }))
  }

  override fun onCleared() {
    super.onCleared()
    speechService?.destroyClient()
    speechService = null
  }
}
