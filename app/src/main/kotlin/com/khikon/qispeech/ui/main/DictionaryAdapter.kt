package com.khikon.qispeech.ui.main

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.khikon.qispeech.R
import com.khikon.qispeech.data.remote.model.Dictionary

const val TYPE_HEADER = 0
const val TYPE_WORD = 1

class DictionaryAdapter : RecyclerView.Adapter<DictionaryAdapter.ViewHolder>() {
  private var items: List<Dictionary>? = null
  private var selectedPosition: Int = NO_INDEX

  fun setDictionaries(list: List<Dictionary>?, selected: Int) {
    this.items = list
    this.selectedPosition = selected + 1 // +1 for header position
    notifyDataSetChanged()
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_dictionary, parent, false))

  override fun getItemCount(): Int = (items?.size ?: 0) + 1

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    when (position) {
      0 -> holder.bindHeader()
      else -> if (items != null) {
        holder.bind(items!![position - 1], selectedPosition == position)
      }
    }
  }

  override fun getItemViewType(position: Int): Int = if (position == 0) TYPE_HEADER else TYPE_WORD

  inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val textLeft: TextView = itemView.findViewById(R.id.text_left)
    private val textRight: TextView = itemView.findViewById(R.id.text_right)
    private val viewGroup: View = itemView.findViewById(R.id.view_item_dictionary)

    fun bind(item: Dictionary, selected: Boolean) {
      textLeft.text = item.word
      textRight.text = "${item.frequency}"

      val colorId = if (selected) R.color.secondaryLightColor else android.R.color.transparent
      viewGroup.setBackgroundColor(itemView.context.getColor(colorId))

      textLeft.setTypeface(textLeft.typeface, Typeface.NORMAL)
      textRight.setTypeface(textRight.typeface, Typeface.NORMAL)
    }

    fun bindHeader() {
      textLeft.setText(R.string.text_dictionary_header_left)
      textRight.setText(R.string.text_dictionary_header_right)

      val colorId = android.R.color.transparent
      viewGroup.setBackgroundColor(itemView.context.getColor(colorId))

      textLeft.setTypeface(textLeft.typeface, Typeface.BOLD)
      textRight.setTypeface(textRight.typeface, Typeface.BOLD)
    }
  }
}
