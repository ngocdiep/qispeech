package com.khikon.qispeech.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.khikon.qispeech.common.ViewModelFactory
import com.khikon.qispeech.common.ViewModelKey
import com.khikon.qispeech.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

  @Binds
  abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

  @Binds
  @IntoMap
  @ViewModelKey(MainViewModel::class)
  abstract fun bindMainViewModel(vm: MainViewModel): ViewModel
}