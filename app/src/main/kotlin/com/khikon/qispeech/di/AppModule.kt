package com.khikon.qispeech.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.khikon.qispeech.BuildConfig
import com.khikon.qispeech.common.SpeechApp
import com.khikon.qispeech.data.moshi
import com.khikon.qispeech.data.remote.RemoteApi
import com.khikon.qispeech.data.remote.RemoteFactory
import com.squareup.moshi.Moshi
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

/**
 * Specific module for app itself
 * */
@Module
abstract class AppModule {
  @Binds
  @ApplicationScoped
  internal abstract fun bindApplication(app: SpeechApp): Application

  @Module
  companion object {
    @JvmStatic
    @Provides
    @ApplicationScoped
    fun bindResources(context: Context): Resources = context.resources

    @JvmStatic
    @Provides
    @ApplicationScoped
    fun provideMoshi(): Moshi = moshi

    @JvmStatic
    @Provides
    @ApplicationScoped
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor = RemoteFactory.provideHttpLoggingInterceptor(BuildConfig.DEBUG)

    @JvmStatic
    @Provides
    @ApplicationScoped
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient = RemoteFactory.provideOkHttpClient(httpLoggingInterceptor)

    @JvmStatic
    @Provides
    @ApplicationScoped
    fun provideRetrofit(client: OkHttpClient, moshi: Moshi): Retrofit = RemoteFactory.provideRetrofit(client, moshi)

    @JvmStatic
    @Provides
    @ApplicationScoped
    fun provideRemoteApi(retrofit: Retrofit): RemoteApi = RemoteFactory.provideRemoteApi(retrofit)
  }
}
