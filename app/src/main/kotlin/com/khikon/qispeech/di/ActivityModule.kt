package com.khikon.qispeech.di

import com.khikon.qispeech.ui.main.MainActivity
import com.khikon.qispeech.ui.main.MainModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

  @ActivityScoped
  @ContributesAndroidInjector(modules = [MainModule::class])
  abstract fun bindMainActivity(): MainActivity
}