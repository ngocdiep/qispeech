package com.khikon.qispeech.di

import android.app.Application
import android.content.Context
import com.khikon.qispeech.common.AppInitial
import com.khikon.qispeech.common.SpeechAppInitial
import com.khikon.qispeech.data.local.db.DictionaryDao
import com.khikon.qispeech.data.local.db.SpeechDatabase
import com.khikon.qispeech.data.log.CrashlyticsReport
import com.khikon.qispeech.data.log.Report
import com.khikon.qispeech.service.speech.SpeechService
import com.khikon.qispeech.service.speech.SpeechServiceImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Common module for general use of application and instrument test
 * */
@Module
abstract class CommonModule {
  @Binds
  @ApplicationScoped
  internal abstract fun bindContext(app: Application): Context

  @Binds
  @ApplicationScoped
  internal abstract fun bindAppInitial(initial: SpeechAppInitial): AppInitial

  @Binds
  @ApplicationScoped
  internal abstract fun bindReport(report: CrashlyticsReport): Report

  @Binds
  @ApplicationScoped
  internal abstract fun provideSpeechService(speechService: SpeechServiceImpl): SpeechService

  @Module
  companion object {
    @JvmStatic
    @Provides
    @ApplicationScoped
    fun provideDatabase(context: Context): SpeechDatabase = SpeechDatabase.getInstance(context)

    @JvmStatic
    @Provides
    @ApplicationScoped
    fun provideDictionaryDao(database: SpeechDatabase): DictionaryDao = database.dictionaryDao()
  }
}