package com.khikon.qispeech.service.speech

import android.content.Context
import com.google.api.gax.rpc.ResponseObserver
import com.google.api.gax.rpc.StreamController
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.speech.v1.*
import com.khikon.qispeech.R
import com.khikon.qispeech.service.recorder.AudioEmitter
import timber.log.Timber
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject

interface SpeechService {
  fun startStream(streamListener: StreamListener?)
  fun pauseStream()
  fun destroyClient()
}

interface StreamListener {
  fun onStreamResponse(value: String)
  fun onStreamError(t: Throwable)
}

/**
 * A customization from https://github.com/GoogleCloudPlatform/android-docs-samples/blob/master/speech/SpeechRecognitionClient/app/src/main/java/com/google/cloud/examples/speechrecognition/MainActivity.kt
 * */
class SpeechServiceImpl @Inject constructor(val context: Context) : SpeechService {
  private var audioEmitter: AudioEmitter? = null

  private val speechClient by lazy {
    // NOTE: The line below uses an embedded credential (res/raw/credential.json).
    //       You should not package a credential with real application.
    //       Instead, you should get a credential securely from a server.
    context.applicationContext.resources.openRawResource(R.raw.credential).use {
      SpeechClient.create(SpeechSettings.newBuilder().setCredentialsProvider {
        GoogleCredentials.fromStream(it)
      }.build())
    }
  }

  override fun startStream(streamListener: StreamListener?) {
    // AtomicBoolean for Threadsafe
    val isFirstRequest = AtomicBoolean(true)
    audioEmitter = AudioEmitter()
    // Start streaming the data to the server and collect responses
    val requestStream = speechClient.streamingRecognizeCallable()
        .splitCall(object : ResponseObserver<StreamingRecognizeResponse> {
          override fun onStart(controller: StreamController?) {
          }

          override fun onResponse(value: StreamingRecognizeResponse?) {
            when {
              value?.resultsCount ?: 0 > 0 -> {
                val transcript = value!!.getResults(0).getAlternatives(0).transcript.trim()
                streamListener?.onStreamResponse(transcript)
              }

              else -> streamListener?.onStreamError(Throwable(value?.error?.message))
            }
          }

          override fun onError(t: Throwable) {
            t.printStackTrace()
            streamListener?.onStreamError(t)
          }

          override fun onComplete() {
            Timber.d("Stream closed!")
          }
        })

    // Monitor the input streamListener and send requests as audio data becomes available
    audioEmitter!!.start { bytes ->
      val builder = StreamingRecognizeRequest.newBuilder().setAudioContent(bytes)

      // If first time, include the config
      if (isFirstRequest.getAndSet(false)) {
        builder.streamingConfig = StreamingRecognitionConfig.newBuilder()
            .setConfig(RecognitionConfig.newBuilder().setLanguageCode("en-US").setEncoding(RecognitionConfig.AudioEncoding.LINEAR16).setSampleRateHertz(16000).build())
            .setInterimResults(false).setSingleUtterance(false).build()
      }

      // send the next request
      requestStream.send(builder.build())
    }
  }

  override fun pauseStream() {
    // Ensure mic data stops
    audioEmitter?.stop()
    audioEmitter = null
  }

  override fun destroyClient() {
    speechClient.shutdown()
  }
}