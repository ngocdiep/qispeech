package com.khikon.qispeech.service.recorder

import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import com.google.protobuf.ByteString
import timber.log.Timber
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Emits microphone audio using a background [ScheduledExecutorService].
 */
class AudioEmitter {
  private var audioRecord: AudioRecord? = null
  private var audioExecutor: ScheduledExecutorService? = null
  private lateinit var buffer: ByteArray

  /** Start streaming  */
  fun start(
      encoding: Int = AudioFormat.ENCODING_PCM_16BIT,
      channel: Int = AudioFormat.CHANNEL_IN_MONO,
      sampleRate: Int = 16000,
      subscriber: (ByteString) -> Unit) {
    audioExecutor = Executors.newSingleThreadScheduledExecutor()

    // Create and configure recorder
    // Note: ensure settings are match the speech recognition config
    releaseAudio()
    audioRecord = AudioRecord.Builder().setAudioSource(MediaRecorder.AudioSource.MIC)
        .setAudioFormat(AudioFormat.Builder()
            .setEncoding(encoding)
            .setSampleRate(sampleRate)
            .setChannelMask(channel)
            .build())
        .build()

    buffer = ByteArray(2 * AudioRecord.getMinBufferSize(sampleRate, channel, encoding))

    // Start recording
    Timber.d("Recording audio with buffer size of: ${buffer.size} bytes")
    audioRecord!!.startRecording()

    // Stream bytes as they become available in chunks equal to the buffer size
    audioExecutor!!.scheduleAtFixedRate({

      // Read audio data
      val read = audioRecord!!.read(buffer, 0, buffer.size, AudioRecord.READ_BLOCKING)

      // Send next chunk
      if (read > 0) {
        subscriber(ByteString.copyFrom(buffer, 0, read))
      }
    }, 0, 10, TimeUnit.MILLISECONDS)
  }

  /** Stop Streaming  */
  fun stop() {
    // Stop events
    audioExecutor?.shutdown()
    audioExecutor = null

    releaseAudio()
  }

  private fun releaseAudio() {
    // Stop recording
    audioRecord?.stop()
    audioRecord?.release()
    audioRecord = null
  }
}