package com.khikon.qispeech.data.repository

import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {
  @Binds
  abstract fun bindDictionaryRepository(repository: DictionaryRepositoryImpl): DictionaryRepository
}
