package com.khikon.qispeech.data

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import se.ansman.kotshi.KotshiJsonAdapterFactory

val moshi: Moshi get() = Moshi.Builder().add(ApplicationJsonAdapterFactory.INSTANCE).build()!!

@KotshiJsonAdapterFactory
abstract class ApplicationJsonAdapterFactory : JsonAdapter.Factory {
  companion object {
    val INSTANCE: ApplicationJsonAdapterFactory = KotshiApplicationJsonAdapterFactory()
  }
}