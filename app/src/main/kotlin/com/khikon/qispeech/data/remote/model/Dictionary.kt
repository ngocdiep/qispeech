package com.khikon.qispeech.data.remote.model

data class DictionaryResponse(val dictionary: List<Dictionary>)

data class Dictionary(var word: String, var frequency: Int)
