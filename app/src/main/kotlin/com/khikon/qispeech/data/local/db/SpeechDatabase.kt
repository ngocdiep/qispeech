package com.khikon.qispeech.data.local.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.khikon.qispeech.data.local.model.DictionaryEntity
import com.khikon.qispeech.util.Constants

@Database(entities = [(DictionaryEntity::class)], version = 1)
abstract class SpeechDatabase : RoomDatabase() {
  abstract fun dictionaryDao(): DictionaryDao

  companion object {
    @Volatile private var INSTANCE: SpeechDatabase? = null

    fun getInstance(context: Context): SpeechDatabase {
      if (INSTANCE == null) {
        synchronized(SpeechDatabase::class.java) {
          if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.applicationContext, SpeechDatabase::class.java,
                Constants.DATABASE_NAME).build()
          }
        }
      }
      return INSTANCE!!
    }
  }
}