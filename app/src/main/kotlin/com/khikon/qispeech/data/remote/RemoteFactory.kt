package com.khikon.qispeech.data.remote

import com.squareup.moshi.Moshi
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit

object RemoteFactory {
  private const val ENDPOINT = "http://a.galactio.com/interview/"

  fun provideHttpLoggingInterceptor(isInternal: Boolean): HttpLoggingInterceptor = HttpLoggingInterceptor().let {
    it.level =
        if (isInternal) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE;it
  }

  fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient = OkHttpClient.Builder().connectionSpecs(
      Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT)).connectTimeout(10,
      TimeUnit.SECONDS).readTimeout(10, TimeUnit.SECONDS).writeTimeout(10,
      TimeUnit.SECONDS).addInterceptor(httpLoggingInterceptor).build()

  fun provideRetrofit(
      client: OkHttpClient, moshi: Moshi): Retrofit = Retrofit.Builder().client(client).baseUrl(
      ENDPOINT).addConverterFactory(MoshiConverterFactory.create(moshi)).addCallAdapterFactory(
      RxJava2CallAdapterFactory.create()).build()

  fun provideRemoteApi(retrofit: Retrofit): RemoteApi = retrofit.create(RemoteApi::class.java)
}