package com.khikon.qispeech.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.khikon.qispeech.data.remote.model.Dictionary
import se.ansman.kotshi.JsonSerializable
import javax.inject.Inject

@JsonSerializable
@Entity(tableName = "dictionaries", indices = [Index("word", unique = true)])
data class DictionaryEntity(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long = 0, @ColumnInfo(
        name = "word") var word: String, @ColumnInfo(name = "frequency") var frequency: Int)

class DictionaryMapper @Inject constructor() {
  fun mapToDomain(list: List<DictionaryEntity>): List<Dictionary> = list.map { mapToDomain(it) }
  fun mapToEntity(list: List<Dictionary>): List<DictionaryEntity> = list.map { mapToEntity(it) }
  fun mapToDomain(e: DictionaryEntity): Dictionary = Dictionary(word = e.word,
      frequency = e.frequency)

  fun mapToEntity(d: Dictionary): DictionaryEntity = DictionaryEntity(word = d.word,
      frequency = d.frequency)
}