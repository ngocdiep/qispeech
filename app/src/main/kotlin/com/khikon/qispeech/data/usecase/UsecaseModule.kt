package com.khikon.qispeech.data.usecase

import dagger.Binds
import dagger.Module

@Module
abstract class UsecaseModule {

  @Binds abstract fun bindDictionaryUsecase(repository: DictionaryUsecaseImpl): DictionaryUsecase
}
