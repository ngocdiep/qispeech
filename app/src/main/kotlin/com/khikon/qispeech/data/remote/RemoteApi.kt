package com.khikon.qispeech.data.remote

import com.khikon.qispeech.data.remote.model.DictionaryResponse
import io.reactivex.Single
import retrofit2.http.GET

interface RemoteApi {
  @GET("dictionary-v2.json") fun getDictionaries(): Single<DictionaryResponse>
}