package com.khikon.qispeech.data.local.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.khikon.qispeech.data.local.model.DictionaryEntity
import io.reactivex.Single

@Dao
interface DictionaryDao {
  @Query("SELECT * FROM dictionaries ORDER BY frequency desc")
  fun getAll(): Single<List<DictionaryEntity>>

  @Query("SELECT * FROM dictionaries WHERE word = :word COLLATE NOCASE")
  fun get(word: String): Single<DictionaryEntity>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(list: List<DictionaryEntity>): List<Long>

  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insert(item: DictionaryEntity): Long

  @Query("DELETE FROM dictionaries")
  fun delete()
}