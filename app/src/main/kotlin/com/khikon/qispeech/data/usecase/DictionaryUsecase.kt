package com.khikon.qispeech.data.usecase

import com.khikon.qispeech.data.remote.model.Dictionary
import com.khikon.qispeech.data.repository.DictionaryRepository
import io.reactivex.Single
import javax.inject.Inject

interface DictionaryUsecase {
  fun fetchDictionaries(): Single<List<Dictionary>>

  fun match(speechWord: String): Single<Dictionary>

  fun plusFrequency(word: String, number: Int): Single<List<Dictionary>>
}

class DictionaryUsecaseImpl @Inject constructor(val dictionaryRepository: DictionaryRepository) : DictionaryUsecase {

  override fun fetchDictionaries() = dictionaryRepository.getAll(true)

  override fun match(speechWord: String): Single<Dictionary> = dictionaryRepository.getItemLocal(speechWord)

  override fun plusFrequency(word: String, number: Int): Single<List<Dictionary>> = dictionaryRepository.plusFrequency(word, number)
      .flatMap { dictionaryRepository.getAll(false) }
}

