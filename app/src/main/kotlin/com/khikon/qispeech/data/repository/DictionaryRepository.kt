package com.khikon.qispeech.data.repository

import com.khikon.qispeech.data.local.db.DictionaryDao
import com.khikon.qispeech.data.local.model.DictionaryEntity
import com.khikon.qispeech.data.local.model.DictionaryMapper
import com.khikon.qispeech.data.remote.RemoteApi
import com.khikon.qispeech.data.remote.model.Dictionary
import io.reactivex.Single
import javax.inject.Inject

interface DictionaryRepository {
  fun getAll(refresh: Boolean): Single<List<Dictionary>>
  fun getItemLocal(word: String): Single<Dictionary>
  fun plusFrequency(word: String, number: Int): Single<Dictionary>
}

class DictionaryRepositoryImpl @Inject constructor(
    private val remoteApi: RemoteApi,
    private val dictionaryDao: DictionaryDao,
    private val dictionaryMapper: DictionaryMapper) : DictionaryRepository {

  override fun getAll(refresh: Boolean): Single<List<Dictionary>> = when (refresh) {
    true -> remoteApi.getDictionaries().flatMap { response ->
      val list = response.dictionary.map {
        // Remove redundant spacing before save to local database
        it.word = it.word.trim()
        it
      }
      saveLocalOverride(list)
    }
    false -> getAllLocal().onErrorResumeNext { getAll(true) }
  }

  override fun getItemLocal(word: String): Single<Dictionary> = dictionaryDao.get(word).map {
    dictionaryMapper.mapToDomain(it)
  }

  override fun plusFrequency(word: String, number: Int): Single<Dictionary> = dictionaryDao.get(
      word).map {
    it.frequency += number.coerceAtLeast(0)
    dictionaryDao.insert(it)
    dictionaryMapper.mapToDomain(it)
  }.onErrorResumeNext {
    dictionaryDao.insert(DictionaryEntity(word = word, frequency = number))
    getItemLocal(word)
  }

  private fun getAllLocal(): Single<List<Dictionary>> = dictionaryDao.getAll().map {
    dictionaryMapper.mapToDomain(it)
  }

  private fun saveLocalOverride(optimizedList: List<Dictionary>): Single<List<Dictionary>> {
    dictionaryDao.insert(dictionaryMapper.mapToEntity(optimizedList))
    return getAllLocal()
  }
}