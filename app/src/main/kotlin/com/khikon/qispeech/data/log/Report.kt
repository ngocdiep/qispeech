package com.khikon.qispeech.data.log

import com.crashlytics.android.Crashlytics
import javax.inject.Inject

interface Report {
  fun logPriority(priority: Int, tag: String?, message: String)
  fun logWarning(t: Throwable)
  fun logError(t: Throwable)
}

class CrashlyticsReport @Inject constructor() : Report {
  override fun logPriority(priority: Int, tag: String?, message: String) = Crashlytics.log(priority,
      tag, message)

  override fun logWarning(t: Throwable) = Crashlytics.logException(t)

  override fun logError(t: Throwable) = Crashlytics.logException(t)
}