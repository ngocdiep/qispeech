package com.khikon.qispeech.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.graphics.drawable.Animatable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import com.khikon.qispeech.R

/**
 * Customized from https://github.com/emrekose26/RecordButton
 * */
class RecordButton : View, Animatable {

  /**
   * Values to draw
   */
  private lateinit var buttonPaint: Paint
  private lateinit var textPaint: Paint
  private lateinit var ripplePaint: Paint
  private lateinit var rectF: RectF

  /**
   * Bitmap for record icon
   */
  private var recordBitmap: Bitmap? = null

  /**
   * Bitmap for pause icon
   */
  private var pauseBitmap: Bitmap? = null

  /**
   * Text hint
   */
  private var textHint: String? = null
  private var textSize: Float = 0F
  private var textColor: Int = 0
  private var minTextPaddingTop: Float = 0F

  /**
   * Record button radius
   */
  var buttonRadius: Float = 0.toFloat()
    private set

  /**
   * Ripple animation radius
   */
  var buttonRipple: Float = 0.toFloat()

  /**
   * Record button fill color
   */
  var buttonColor: Int = 0

  /**
   * Record icon res id
   */
  var recordIcon: Int = 0

  /**
   * Pause icon res id
   */
  var pauseIcon: Int = 0

  private var isRecording = false

  /**
   * Listener that notify on record and record finish
   */
  private var recordListener: OnRecordListener? = null

  constructor(context: Context) : super(context) {
    init(context, null)
  }

  constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
    init(context, attrs)
  }

  constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
    init(context, attrs)
  }

  @SuppressLint("NewApi") constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
    init(context, attrs)
  }

  /**
   * Initialize view
   * @param context
   * @param attrs
   */
  private fun init(context: Context, attrs: AttributeSet?) {
    val resource = context.obtainStyledAttributes(attrs, R.styleable.RecordButton)
    buttonRadius = resource.getDimension(R.styleable.RecordButton_buttonRadius, resources.displayMetrics.scaledDensity * 40f)
    buttonRipple = resource.getDimension(R.styleable.RecordButton_buttonRipple, resources.displayMetrics.scaledDensity * 12f)

    buttonColor = resource.getColor(R.styleable.RecordButton_buttonColor, Color.RED)
    recordIcon = resource.getResourceId(R.styleable.RecordButton_recordIcon, -1)
    pauseIcon = resource.getResourceId(R.styleable.RecordButton_pauseIcon, -1)

    textHint = resource.getString(R.styleable.RecordButton_textHint)
    textColor = resource.getColor(R.styleable.RecordButton_textColor, Color.LTGRAY)
    textSize = resource.getDimension(R.styleable.RecordButton_textSize, context.resources.getDimension(R.dimen.abc_text_size_small_material))
    minTextPaddingTop = context.resources.getDimension(R.dimen.spacing_small)
    resource.recycle()

    buttonPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    buttonPaint.color = buttonColor
    buttonPaint.style = Paint.Style.FILL

    ripplePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    ripplePaint.color = ColorUtils.setAlphaComponent(buttonColor, 30)
    ripplePaint.style = Paint.Style.FILL

    textPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    textPaint.color = textColor
    textPaint.textAlign = Paint.Align.CENTER
    textPaint.textSize = textSize

    rectF = RectF()

    recordBitmap = getBitmapFromVectorDrawable(context, recordIcon)
    pauseBitmap = getBitmapFromVectorDrawable(context, pauseIcon)
  }

  override fun onDraw(canvas: Canvas) {
    super.onDraw(canvas)

    val cx = width / 2
    val cy = height / 2

    // Draw ripple and main circle
    canvas.drawCircle(cx.toFloat(), cy.toFloat(), buttonRadius + buttonRipple, ripplePaint)
    canvas.drawCircle(cx.toFloat(), cy.toFloat(), buttonRadius, buttonPaint)

    // Draw bitmap icon
    val bitmap = if (isRecording) pauseBitmap else recordBitmap
    drawBitmap(canvas, bitmap, cx, cy)

    // Draw hint text
    drawText(canvas)
  }

  private fun drawBitmap(canvas: Canvas, bitmap: Bitmap?, cx: Int, cy: Int) {
    if (bitmap != null) {
      // Draw from center x and center y
      canvas.drawBitmap(bitmap, (cx - bitmap.height / 2).toFloat(), (cy - bitmap.width / 2).toFloat(), null)
    }
  }

  private fun drawText(canvas: Canvas) {
    if (textHint != null && !isRecording) {
      val rect = Rect()
      textPaint.getTextBounds(textHint, 0, textHint!!.length, rect)

      val xPos = canvas.width / 2F
      val yPos = (canvas.height / 2F - buttonRipple - buttonRadius) / 2 + minTextPaddingTop

      // Draw from center x, and middle y of top space
      canvas.drawText(textHint!!, xPos, yPos, textPaint)
    }
  }

  override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
    val size = buttonRadius.toInt() * 2 + buttonRipple.toInt() * 2 + 40

    val widthMode = View.MeasureSpec.getMode(widthMeasureSpec)
    val widthSize = View.MeasureSpec.getSize(widthMeasureSpec)
    val heightMode = View.MeasureSpec.getMode(heightMeasureSpec)
    val heightSize = View.MeasureSpec.getSize(heightMeasureSpec)

    val width: Int
    val height: Int

    width = when (widthMode) {
      View.MeasureSpec.EXACTLY -> widthSize
      View.MeasureSpec.AT_MOST -> Math.min(size, widthSize)
      View.MeasureSpec.UNSPECIFIED -> size
      else -> size
    }

    height = when (heightMode) {
      View.MeasureSpec.EXACTLY -> heightSize
      View.MeasureSpec.AT_MOST -> Math.min(size, heightSize)
      View.MeasureSpec.UNSPECIFIED -> size
      else -> size
    }

    setMeasuredDimension(width, height)
  }

  override fun performClick(): Boolean = true

  override fun onTouchEvent(event: MotionEvent): Boolean {
    if (isInsideButton(event.x, event.y)) {
      when (event.action) {
        MotionEvent.ACTION_DOWN -> {
          if (isRecording) {
            stop()
            recordListener?.onRecordCancel()
          } else {
            start()
            recordListener?.onRecordStart()
          }
          performClick()
          postInvalidate()
          return true
        }
      }
    }
    return false
  }

  fun isInsideButton(x: Float, y: Float): Boolean {
    val centerX = width / 2
    val centerY = height / 2

    return x >= centerX - buttonRadius && x <= centerX + buttonRadius
        && y >= centerY - buttonRadius && y <= centerY + buttonRadius
  }

  /**
   * Record button scale animation starting
   */
  override fun start() {
    isRecording = true
    scaleAnimation(1.1f, 1.1f)
  }

  /**
   * Record button scale animation stopping
   */
  override fun stop() {
    isRecording = false

    scaleAnimation(1f, 1f)
  }

  override fun isRunning(): Boolean = isRecording

  /**
   * This method provides scale animation to view
   * between scaleX and scale Y values
   * @param scaleX
   * @param scaleY
   */
  private fun scaleAnimation(scaleX: Float, scaleY: Float) {
    // TODO: Ripple animation
    //    this.animate().scaleX(scaleX).scaleY(scaleY).start()
  }

  fun setRecordListener(recordListener: OnRecordListener) {
    this.recordListener = recordListener
  }

  private fun getBitmapFromVectorDrawable(context: Context, drawableId: Int): Bitmap {
    val drawable = ContextCompat.getDrawable(context, drawableId)

    val bitmap = Bitmap.createBitmap(drawable!!.intrinsicWidth, drawable.intrinsicHeight,
        Bitmap.Config.ARGB_8888)
    val canvas = Canvas(bitmap)
    drawable.setBounds(0, 0, canvas.width, canvas.height)
    drawable.draw(canvas)

    return bitmap
  }

  fun cancelRecord() {
    stop()
    postInvalidate()
  }

  interface OnRecordListener {
    fun onRecordStart()
    fun onRecordCancel()
  }
}