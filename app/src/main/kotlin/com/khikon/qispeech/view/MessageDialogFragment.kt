package com.khikon.qispeech.view

import android.R
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatDialogFragment
import com.khikon.qispeech.util.putArgs

class MessageDialogFragment : AppCompatDialogFragment() {
  private var listener: Listener? = null

  override fun onCreateDialog(savedInstanceState: Bundle?): Dialog = AlertDialog.Builder(context!!)
      .setMessage(arguments!!.getString(ARG_MESSAGE))
      .setPositiveButton(R.string.ok) { _, _ -> listener?.onPositiveButtonClicked() }
      .create()

  override fun onCancel(dialog: DialogInterface?) {
    super.onCancel(dialog)
    listener?.onMessageDialogCancelled()
  }

  fun setDialogListener(listener: Listener): MessageDialogFragment {
    this.listener = listener
    return this
  }

  companion object {
    private const val ARG_MESSAGE = "message"

    fun newInstance(message: String): MessageDialogFragment = MessageDialogFragment().putArgs { putString(ARG_MESSAGE, message) }
  }

  interface Listener {
    /**
     * Called when positive button clicked.
     * */
    fun onPositiveButtonClicked()

    /**
     * Called when the dialog is cancelled.
     */
    fun onMessageDialogCancelled()
  }
}