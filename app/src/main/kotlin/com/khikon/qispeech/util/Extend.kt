package com.khikon.qispeech.util

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.*
import com.khikon.qispeech.data.moshi

val <T : Any> T.TAG: String get() = javaClass.simpleName

inline fun <F : Fragment> F.putArgs(argsBuilder: Bundle.() -> Unit): F = this.apply {
  arguments = Bundle().apply(argsBuilder)
}

/** Extend support for ViewModel and Lifecycle */
inline fun <reified T : ViewModel> FragmentActivity.getViewModel(viewModelFactory: ViewModelProvider.Factory): T = ViewModelProviders.of(
    this, viewModelFactory)[T::class.java]

inline fun <reified T : ViewModel> FragmentActivity.withViewModel(
    viewModelFactory: ViewModelProvider.Factory, body: T.() -> Unit): T = getViewModel<T>(
    viewModelFactory).let { it.body();it }

inline fun <reified T : ViewModel> Fragment.getViewModel(viewModelFactory: ViewModelProvider.Factory): T = ViewModelProviders.of(
    this, viewModelFactory)[T::class.java]

inline fun <reified T : ViewModel> Fragment.withViewModel(
    viewModelFactory: ViewModelProvider.Factory, body: T.() -> Unit): T = getViewModel<T>(
    viewModelFactory).let { it.body();it }

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(
    liveData: L, body: (T?) -> Unit) = liveData.observe(this, Observer(body))

/** Moshi support */
inline fun <reified C : Any> C.toJson(): String = moshi.adapter<C>(C::class.java).toJson(this)

inline fun <reified C : Any> fromJson(jsonString: String): C? = moshi.adapter<C>(C::class.java).fromJson(jsonString)