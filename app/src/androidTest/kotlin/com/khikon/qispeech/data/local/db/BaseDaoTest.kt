package com.khikon.qispeech.data.local.db

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.Rule

abstract class BaseDaoTest<T : Any> {
  @get:Rule var instantTaskExecutorRule = InstantTaskExecutorRule()

  protected lateinit var database: SpeechDatabase
  protected lateinit var dao: T

  @Before @Throws(Exception::class) fun initDb() {
    database = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
        SpeechDatabase::class.java)
        // Allowing main thread queries, just for testing
        .allowMainThreadQueries().build()
    dao = initDao()
  }

  abstract fun initDao(): T

  @After @Throws(Exception::class) fun closeDb() {
    database.close()
  }
}