package com.khikon.qispeech.data.local.db

import androidx.test.runner.AndroidJUnit4
import com.khikon.qispeech.data.local.model.DictionaryEntity
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DictionaryDaoTest : BaseDaoTest<DictionaryDao>() {

  private val dictionaryEntity = DictionaryEntity(3, "hello", 4)
  override fun initDao(): DictionaryDao = database.dictionaryDao()

  @Test
  fun testGetAllWhenNoDataInserted() {
    val test = dao.getAll().test()
    test.assertNoErrors()
    test.assertComplete()
    test.assertValue { it.isEmpty() }
  }

  @Test
  fun testInsertAndGet() {
    // When inserting a new dictionary in the data source
    dao.insert(dictionaryEntity)

    // The dictionary can be retrieved
    val test = dao.get(dictionaryEntity.word).test()
    test.assertNoErrors()
    test.assertComplete()
    test.assertValue {
      dictionaryEntity.id == it.id && dictionaryEntity.word == it.word && dictionaryEntity.frequency == it.frequency
    }
  }

  @Test
  fun testInsertGetIgnoreCase() {
    // When inserting a new dictionary in the data source
    dao.insert(dictionaryEntity)

    // The dictionary can be retrieved
    val test = dao.get(dictionaryEntity.word.toUpperCase()).test()
    test.assertNoErrors()
    test.assertComplete()
    test.assertValue {
      dictionaryEntity.id == it.id && dictionaryEntity.word.equals(it.word, true) && dictionaryEntity.frequency == it.frequency
    }
  }

  @Test
  fun testInsertOverride() {
    // When inserting a new dictionary in the data source
    val copiedEntity = dictionaryEntity.copy(id = 99, frequency = 100)
    dao.insert(dictionaryEntity)
    dao.insert(copiedEntity)

    // The retrieved dictionary should be the copied entity
    val test = dao.get(dictionaryEntity.word).test()
    test.assertNoErrors()
    test.assertComplete()
    test.assertValue {
      copiedEntity.id == it.id && copiedEntity.word == it.word && copiedEntity.frequency == it.frequency
    }
  }

  @Test
  fun testInsertAndGetAll() {
    // When inserting new dictionaries in the data source
    val list = listOf(dictionaryEntity, dictionaryEntity.copy(id = 0, word = "hi"))
    dao.insert(list)

    // The dictionary can be retrieved
    val test = dao.getAll().test()
    test.assertNoErrors()
    test.assertComplete()
    test.assertValueCount(1) // One list
    test.assertValue {
      var assertion = true
      list.forEachIndexed { index, item ->
        assertion = assertion && item.word == it[index].word
        assertion = assertion && item.frequency == it[index].frequency
      }
      assertion
    }
  }

  @Test
  fun testDeleteAndGet() {
    // Given that we have a dictionary in the data source
    dao.insert(dictionaryEntity)

    // When we are deleting all
    dao.delete()

    // When subscribing to the emissions of the user
    val test = dao.getAll().test()
    test.assertNoErrors()
    test.assertComplete()
    test.assertValue { it.isEmpty() }
  }
}