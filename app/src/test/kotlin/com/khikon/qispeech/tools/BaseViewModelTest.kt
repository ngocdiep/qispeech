package com.khikon.qispeech.tools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Rule
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import org.mockito.junit.MockitoJUnit

abstract class BaseViewModelTest {
  @get:Rule
  val mockitoRule = MockitoJUnit.rule()!!

  @get:Rule
  val taskExecutorRule = InstantTaskExecutorRule()

  @get:Rule
  val rxSchedulerRule = RxSchedulerRule()
}

class RxSchedulerRule : TestRule {

  override fun apply(base: Statement, description: Description) =
      object : Statement() {
        override fun evaluate() {
          RxAndroidPlugins.reset()
          RxAndroidPlugins.setInitMainThreadSchedulerHandler { SCHEDULER_INSTANCE }

          RxJavaPlugins.reset()
          RxJavaPlugins.setIoSchedulerHandler { SCHEDULER_INSTANCE }
          RxJavaPlugins.setNewThreadSchedulerHandler { SCHEDULER_INSTANCE }
          RxJavaPlugins.setComputationSchedulerHandler { SCHEDULER_INSTANCE }

          base.evaluate()
        }
      }

  companion object {
    private val SCHEDULER_INSTANCE = Schedulers.trampoline()
  }
}