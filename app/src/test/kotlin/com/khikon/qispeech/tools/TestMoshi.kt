package com.khikon.qispeech.tools

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import se.ansman.kotshi.KotshiJsonAdapterFactory

val testMoshi: Moshi get() = Moshi.Builder().add(ApplicationJsonAdapterFactory.INSTANCE).build()!!

@KotshiJsonAdapterFactory
abstract class ApplicationJsonAdapterFactory : JsonAdapter.Factory {
  companion object {
    val INSTANCE: ApplicationJsonAdapterFactory = KotshiApplicationJsonAdapterFactory()
  }
}