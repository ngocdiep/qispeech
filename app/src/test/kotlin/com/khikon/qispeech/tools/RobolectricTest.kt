package com.khikon.qispeech.tools

import android.os.Build
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
//@PowerMockIgnore("org.mockito.*", "org.robolectric.*", "android.*", "androidx.")
// Use sdk 27 because Robolectric's stable version didn't support 28 yet
@Config(sdk = [Build.VERSION_CODES.O_MR1])
abstract class RobolectricTest {

  //  @Rule
  //  @JvmField
  //  var rule = PowerMockRule()

}