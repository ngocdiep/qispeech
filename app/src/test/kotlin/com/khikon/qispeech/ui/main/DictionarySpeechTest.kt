package com.khikon.qispeech.ui.main

import com.google.common.truth.Truth.assertThat
import com.khikon.qispeech.data.remote.model.Dictionary
import org.junit.Before
import org.junit.Test

class DictionarySpeechTest {

  private val word = "hello"
  private val wordCap = "Hello"
  private val wordSpace = " hello "
  private val speechSuccess = Speech(word, null)
  private val speechError = Speech(null, "Error occurred!")

  @Before
  fun init() {
  }

  private fun assertMatch(word: String, speech: Speech, assertIndex: Int) {
    val dictionaryList = listOf(Dictionary(word, 4))
    val dictionarySpeech = DictionarySpeech(dictionaryList, speech)

    assertThat(dictionarySpeech.matchedPosition).isEqualTo(assertIndex)
  }

  @Test
  fun testMatchSuccessSpeech() {
    assertMatch(word, speechSuccess, 0)
  }

  @Test
  fun testMatchErrorSpeech() {
    assertMatch(word, speechError, NO_INDEX)
  }

  @Test
  fun testMatchWordCap() {
    assertMatch(word, Speech(wordCap, null), 0)
  }

  @Test
  fun testMatchWordCapReverse() {
    assertMatch(wordCap, Speech(word, null), 0)
  }

  @Test
  fun testMatchWordSpace() {
    assertMatch(word, Speech(wordSpace, null), 0)
  }

  @Test
  fun testMatchWordSpaceReverse() {
    assertMatch(wordSpace, Speech(word, null), 0)
  }
}