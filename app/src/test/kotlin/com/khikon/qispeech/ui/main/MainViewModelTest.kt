package com.khikon.qispeech.ui.main

import android.content.res.Resources
import com.google.common.truth.Truth.assertThat
import com.khikon.qispeech.data.remote.model.Dictionary
import com.khikon.qispeech.data.usecase.DictionaryUsecase
import com.khikon.qispeech.service.speech.SpeechService
import com.khikon.qispeech.tools.BaseViewModelTest
import com.khikon.qispeech.tools.testObserver
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class MainViewModelTest : BaseViewModelTest() {

  private val resources: Resources = mock()
  private val usecase: DictionaryUsecase = mock()
  private val speechService: SpeechService? = mock()

  private lateinit var mainViewModel: MainViewModel

  private val dictionary = Dictionary("hello", 7)

  @Before
  fun init() {
    mainViewModel = MainViewModel(resources, usecase, speechService)
  }

  @Test
  fun testInitLiveData_ShouldEmpty() {
    val liveData = mainViewModel.dictionarySpeechLiveData.testObserver()

    assertThat(liveData.observedValues).isEqualTo(emptyList<DictionarySpeech>())
  }

  @Test
  fun testFetchDictionaryList_Success() {
    // Given
    val dictionaryList = listOf(dictionary)
    whenever(usecase.fetchDictionaries()).thenReturn(Single.just(dictionaryList))
    val liveData = mainViewModel.dictionarySpeechLiveData.testObserver()

    // When
    mainViewModel.fetchDictionaryList(false)

    // Then
    verify(usecase).fetchDictionaries()

    val expected = DictionarySpeech(dictionaryList, null)
    val value = liveData.observedValues[0]!!
    assertEquals(expected.dictionaryList, value.dictionaryList)
    assertEquals(expected.speech, value.speech)
    assertEquals(expected.matchedPosition, value.matchedPosition)
  }

  @Test
  fun testPlusFrequency_Success() {
    // Given
    val dictionary = Dictionary("hello", 4)
    val list = listOf(dictionary)
    whenever(usecase.plusFrequency(dictionary.word, 1)).thenReturn(Single.just(list))
    val liveData = mainViewModel.dictionarySpeechLiveData.testObserver()

    // When
    mainViewModel.plusFrequency(dictionary.word)

    // Then
    verify(usecase).plusFrequency(dictionary.word, 1)

    val expected = DictionarySpeech(list, null)
    val value = liveData.observedValues[0]!!
    assertEquals(expected.dictionaryList, value.dictionaryList)
    assertEquals(expected.speech, value.speech)
    assertEquals(expected.matchedPosition, value.matchedPosition)
  }
}