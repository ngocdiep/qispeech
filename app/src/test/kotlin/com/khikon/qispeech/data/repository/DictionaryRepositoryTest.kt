package com.khikon.qispeech.data.repository

import com.khikon.qispeech.data.local.db.DictionaryDao
import com.khikon.qispeech.data.local.model.DictionaryEntity
import com.khikon.qispeech.data.local.model.DictionaryMapper
import com.khikon.qispeech.data.remote.RemoteApi
import com.khikon.qispeech.data.remote.model.Dictionary
import com.khikon.qispeech.data.remote.model.DictionaryResponse
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class DictionaryRepositoryTest {
  private lateinit var repository: DictionaryRepositoryImpl
  private val mockApi = mock<RemoteApi> {}
  private val mockDao = mock<DictionaryDao>()
  private val mapper = DictionaryMapper()

  private val remoteItem = Dictionary("hello  ", 3)
  private val daoItem = mapper.mapToEntity(remoteItem).copy(word = "hello")

  private val remoteList = listOf(remoteItem)
  private val daoList = listOf(daoItem)

  private val throwable = Throwable()

  @Before fun setUp() {
    repository = DictionaryRepositoryImpl(mockApi, mockDao, mapper)
  }

  @Test fun testGetAllDao_Success() {
    // Given
    whenever(mockDao.getAll()).thenReturn(Single.just(daoList))

    // When
    val test = repository.getAll(false).test()

    // Then
    Mockito.verify(mockDao).getAll()
    test.assertValue(mapper.mapToDomain(daoList))
  }

  @Test fun testGetAllDao_Fail_FallbackRemote_Succeed() {
    // Given
    val entityList = mapper.mapToEntity(remoteList)
    entityList.map { it.word = it.word.trim() }

    whenever(mockDao.getAll()).thenReturn(Single.error(throwable), Single.just(entityList))
    whenever(mockApi.getDictionaries()).thenReturn(Single.just(DictionaryResponse(remoteList)))

    // When
    val test = repository.getAll(false).test()

    // Then
    Mockito.verify(mockDao, Mockito.times(2)).getAll()
    Mockito.verify(mockApi).getDictionaries()
    Mockito.verify(mockDao).insert(entityList)
    test.assertValue(mapper.mapToDomain(entityList))
  }

  @Test fun testGetAllDao_Fail_FallbackRemote_Fail() {
    // Given
    whenever(mockDao.getAll()).thenReturn(Single.error(throwable),
        Single.just(mapper.mapToEntity(remoteList)))
    whenever(mockApi.getDictionaries()).thenReturn(Single.error(throwable))

    // When
    val test = repository.getAll(false).test()

    // Then
    Mockito.verify(mockDao).getAll()
    Mockito.verify(mockApi).getDictionaries()
    test.assertError(throwable)
  }

  @Test fun testGetAllRemote_Success() {
    // Given
    whenever(mockApi.getDictionaries()).thenReturn(Single.just(DictionaryResponse(remoteList)))
    whenever(mockDao.getAll()).thenReturn(Single.just(daoList))

    // When
    val test = repository.getAll(true).test()

    // Then
    Mockito.verify(mockApi).getDictionaries()
    Mockito.verify(mockDao).insert(daoList)
    Mockito.verify(mockDao).getAll()
    test.assertValue(mapper.mapToDomain(daoList))
  }

  @Test fun testGetAllRemote_Fail() {
    // Given
    whenever(mockApi.getDictionaries()).thenReturn(Single.error(throwable))

    // When
    val test = repository.getAll(true).test()

    // Then
    Mockito.verify(mockApi).getDictionaries()
    test.assertError(throwable)
  }

  @Test fun testGetItemDao_Success() {
    // Given
    whenever(mockDao.get(daoItem.word)).thenReturn(Single.just(daoItem))

    // When
    val test = repository.getItemLocal(daoItem.word).test()

    // Then
    Mockito.verify(mockDao).get(daoItem.word)
    test.assertValue(mapper.mapToDomain(daoItem))
  }

  @Test fun testGetItemDao_Fail() {
    // Given
    whenever(mockDao.get(daoItem.word)).thenReturn(Single.error(throwable))

    // When
    val test = repository.getItemLocal(daoItem.word).test()

    // Then
    Mockito.verify(mockDao).get(daoItem.word)
    test.assertError(throwable)
  }

  @Test fun testPlusFrequency_Positive() {
    assertPlusFrequency(daoItem, 5, daoItem.frequency + 5)
  }

  @Test fun testPlusFrequency_Zero() {
    assertPlusFrequency(daoItem, 0, daoItem.frequency)
  }

  @Test fun testPlusFrequency_Negative() {
    assertPlusFrequency(daoItem, -4, daoItem.frequency)
  }

  private fun assertPlusFrequency(returnItem: DictionaryEntity, plus: Int, newFrequency: Int) {
    // Given
    whenever(mockDao.get(returnItem.word)).thenReturn(Single.just(returnItem))

    // When
    val test = repository.plusFrequency(returnItem.word, plus).test()

    // Then
    verify(mockDao).get(returnItem.word)
    verify(mockDao).insert(returnItem)
    test.assertValue { newFrequency == it.frequency }
  }

  @Test fun testPlusFrequency_EmptyWord() {
    // Given
    val entity = DictionaryEntity(word = "any string", frequency = 5)
    whenever(mockDao.get(entity.word)).thenReturn(Single.error(throwable), Single.just(entity))

    // When
    val test = repository.plusFrequency(entity.word, entity.frequency).test()

    // Then
    verify(mockDao, Mockito.times(2)).get(entity.word)
    verify(mockDao).insert(entity)
    test.assertValue { entity.frequency == it.frequency }
  }
}