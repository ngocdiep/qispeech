package com.khikon.qispeech.data.local.model

import com.khikon.qispeech.data.remote.model.Dictionary
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class DictionaryMapperTest {
  private lateinit var mapper: DictionaryMapper

  private val id = 3L
  private val word = " hello    "
  private val frequency = 4

  @Before fun setUp() {
    mapper = DictionaryMapper()
  }

  @Test fun testMapEntityToDomain() {
    // Given
    val entity = DictionaryEntity(id, word, frequency)

    // When
    val model = mapper.mapToDomain(entity)

    // Then
    Assert.assertTrue(model.word == entity.word)
    Assert.assertTrue(model.frequency == entity.frequency)
  }

  @Test fun testMapDomainToEntity() {
    // Given
    val model = Dictionary(word, frequency)

    // When
    val entity = mapper.mapToEntity(model)

    // Then
    Assert.assertTrue(entity.word == model.word)
    Assert.assertTrue(entity.frequency == model.frequency)
  }
}