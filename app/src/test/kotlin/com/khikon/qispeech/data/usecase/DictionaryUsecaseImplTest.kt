package com.khikon.qispeech.data.usecase

import com.khikon.qispeech.data.remote.model.Dictionary
import com.khikon.qispeech.data.repository.DictionaryRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class DictionaryUsecaseImplTest {
  private lateinit var usecase: DictionaryUsecaseImpl
  private val repo: DictionaryRepository = mock()

  @Before
  fun setUp() {
    usecase = DictionaryUsecaseImpl(repo)
  }

  @Test
  fun testPlusFrequency() {

  }

  @Test
  fun testPlusFrequency_Success() {
    // Given
    val entity = Dictionary(word = "any string", frequency = 5)
    whenever(repo.plusFrequency(entity.word, entity.frequency)).thenReturn(Single.just(entity))
    whenever(repo.getAll(false)).thenReturn(Single.just(listOf(entity)))

    // When
    val test = usecase.plusFrequency(entity.word, entity.frequency).test()

    // Then
    verify(repo).plusFrequency(entity.word, entity.frequency)
    verify(repo).getAll(false)
    test.assertValue {
      var assertion = true
      for (item in it) {
        if (entity.word == item.word) assertion = assertion && entity.frequency == item.frequency
      }
      assertion
    }
  }
}